BINDIR=~/bin/

.PHONY: build
build:
	cargo build --release

.PHONY: run
run:
	cargo run

.PHONY: clean
clean:
	cargo clean

.PHONY: install
install: build
	cp -rv target/release/cpulimitd ${BINDIR}
