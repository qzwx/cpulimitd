use std::io::{self, Write};
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::{thread, time};
use std::process::{Command, Stdio};
use serde::{Deserialize, Serialize};
use fnv::FnvHashMap;
use std::env;

#[derive(Serialize, Deserialize)]
struct ConfigEntry
{
	cmd: String,
	params: Vec<String>
}

#[derive(Serialize, Deserialize)]
struct Config
{
	cmd: String,
	sleeptime: u64,
	statusdots: bool,
	limits: Vec<ConfigEntry>
}

struct ConfData
{
	config: Config,
	path: String,
	mtime: std::time::SystemTime
}

fn run_cpulimit(pid: u32, exe: &str, cpulimit_exe: &String, params: &Vec<String>, child_map: &mut FnvHashMap::<u32, std::process::Child>) -> std::io::Result<u32>
{
	let mut cmd = Command::new(cpulimit_exe);
	cmd.stdin(Stdio::null())
		.stdout(Stdio::null())
		.stderr(Stdio::null())
		.arg("-p")
		.arg(pid.to_string());
	for param in params.iter()
	{
		cmd.arg(param);
	}
	let child = cmd.spawn()?;
	println!("Spawned cpulimit with pid {} for process {} {}", child.id(), exe, pid);
	let child_pid = child.id();
	child_map.insert(child_pid, child);
	Ok(child_pid)
}

fn check_procs(config: &Config, pid_map: &FnvHashMap::<u32, u32>, child_map: &mut FnvHashMap::<u32, std::process::Child>) -> FnvHashMap::<u32, u32>
{
	let mut new_pid_map = FnvHashMap::<u32, u32>::default();
	for entry in fs::read_dir("/proc").expect("Couldn't read /proc")
	{
		let entry = entry.expect("Couldn't get entry from /proc");
		let pid = entry.file_name();

		let pid: u32 = match pid.to_string_lossy().trim().parse()
		{
			Ok(num) => num,
			Err(_) => continue, // ignore if the folder is not a number
		};

		let child_pid = pid_map.get(&pid);
		if child_pid.is_some()
		{
			let child_pid = child_pid.unwrap();
			// eprintln!("Keeping pid {} with child pid {}", pid, child_pid);
			new_pid_map.insert(pid, *child_pid);
			continue;
		}

		let mut path = entry.path();
		path.push("exe");
		let linkpath = match fs::read_link(path)
		{
			Ok(linkpath) => linkpath,
			Err(_) => continue, // probably not enough privileges to see this
		};

		for cfg_entry in config.limits.iter()
		{
			let cmd = String::from("/") + &cfg_entry.cmd;
			if linkpath.to_string_lossy().ends_with(&cmd)
			{
				let child_pid = run_cpulimit(pid, &linkpath.to_string_lossy(), &config.cmd, &cfg_entry.params, child_map);
				if child_pid.is_ok()
				{
					new_pid_map.insert(pid, child_pid.unwrap());
				}
				else
				{
					eprintln!("Failed to spawn cpulimit for process {}", pid);
				}
				break;
			}
		}
	}
	for (pid, child_pid) in pid_map.iter()
	{
		if !new_pid_map.contains_key(pid)
		{
			// println!("Wait for child {}", child_pid);
			let mut child = child_map.remove(child_pid).expect("Error, missing child");
			let status = child.wait().expect("Error while waiting for child to die");
			match status.code() {
				Some(code) => println!("Cpulimit {} exited with status code: {}", child_pid, code),
				None       => println!("Cpulimit process terminated by signal")
			}
		}
	}
	new_pid_map
}

fn read_config() -> ConfData
{
	let mut path = String::from("./config.json");
	let metadata;
	match fs::metadata(&path)
	{
		Ok(md) => metadata = md,
		Err(_md) =>
		{
			let home = env::var("HOME").expect("Can't find config file.");
			path = home + "/.cpulimitd.json";
			metadata = fs::metadata(&path).expect("Can't stat config file");
		}
	}
	let modtime = metadata.modified().expect("Can't read modified time of config file");
	let file = File::open(&path);
	let mut file = file.expect("Can't find config file.");
	let mut contents = String::new();
	file.read_to_string(&mut contents).expect("Can't read config file.");
	let c: Config = serde_json::from_str(&contents).expect("Invalid json in config file");
	ConfData{config: c, path: path, mtime: modtime}
}

fn main()
{
	println!("cpulimitd");
	//let config = read_config();
	let mut conf_data = read_config();
	let mut pid_map = FnvHashMap::<u32, u32>::default();
	let mut child_map = FnvHashMap::<u32, std::process::Child>::default();
	loop
	{
		if conf_data.config.statusdots
		{
			eprint!(".");
			io::stderr().flush().unwrap();
		}
		let metadata = fs::metadata(&conf_data.path).expect("Can't stat config file");
		let newmtime = metadata.modified().expect("Can't read modified time of config file");
		if newmtime > conf_data.mtime
		{
			conf_data = read_config();
		}
		pid_map = check_procs(&conf_data.config, &pid_map, &mut child_map);
		let sleeptime = time::Duration::from_millis(conf_data.config.sleeptime);
		thread::sleep(sleeptime);
	}
}
