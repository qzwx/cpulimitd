# cpulimitd

A simple utility to automate the starting of cpulimit.

cpulimitd will look for a file named `config.json` in the working directory or, if that file doesn't exist for `~/.cpulimitd.json`.

An example config file is included with this code. Basically it contains a command that should be run (`cpulimit`), a timeout in miliseconds that cpulimitd will sleep between checking the running processes, and a list of commands that should be limited with options to pass to cpulimit.


## license

Copyright © 2018 Hans Alves <halves@qzwx.nl>

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
